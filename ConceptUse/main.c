#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sqlite3.h>

#define BUFFER_SIZE 1024

int fillData()
{
    sqlite3 *myDB = NULL;

    /*关闭野指针*/
    const char *filename = "./test.db";

    int ret = sqlite3_open(filename, &myDB);
    if (ret != SQLITE_OK)
    {
        // sqlite3_errmsg()
        printf("sqlite3_open error:%s\n", sqlite3_errmsg(myDB));
        exit(-1);
    }

    /**新建数据库表 userInfo*/
    char sql[BUFFER_SIZE] = "create table IF NOT EXISTS userInfo( \
                        userID int primary key not null,\
                        username text not null,\
                        passwd text not null \
                        );";
    ret = sqlite3_exec(myDB, sql, NULL, NULL, NULL);
    if (ret != SQLITE_OK)
    {
        printf("sqlite3_exec error:%s\n", sqlite3_errmsg(myDB));
        exit(-1);
    }

    /*需求：手动输入ID & 用户名 & 密码 放到数据库里面去 */
    int id = 333;
    char *name = "bao";
    char *passwd = "199812";

    /*插入数据*/
    // sql = "insert into userInfo values (222, 'wan', '199812');";
    char m_sql[BUFFER_SIZE] = {0};
    sprintf(sql, "insert into userInfo values (%d, '%s', '%s')", id, name, passwd);

    sqlite3_exec(myDB, sql, NULL, NULL, NULL);
    if (ret != SQLITE_OK)
    {
        printf("sqlite3_exec error:%s\n", sqlite3_errmsg(myDB));
        exit(-1);
    }

    /*关闭数据库*/
    sqlite3_close(myDB);
}

int demoSelect()
{
    sqlite3 *myDB = NULL;

    /*关闭野指针*/
    const char *filename = "./test.db";

    int ret = sqlite3_open(filename, &myDB);
    if (ret != SQLITE_OK)
    {
        // sqlite3_errmsg()
        printf("sqlite3_open error:%s\n", sqlite3_errmsg(myDB));
        exit(-1);
    }

    const char *sql = "select * from userInfo";
    int row = 0;
    int column = 0;
    char **result = NULL;
    /*char *result[]*/
    ret = sqlite3_get_table(myDB, sql, &result, &row, &column, NULL);

    if (ret != SQLITE_OK)
    {
        printf("sqlite3_get_table error:%s\n", sqlite3_errmsg(myDB));
        exit(-1);
    }
    printf("row:%d,\t column: %d\n", row, column);

    for (int idx = 0; idx <= row; idx++)
    {
        for (int jdx = 0; jdx < column; jdx++)
        {
            printf("result[%d]:%-20s\t",idx * column + jdx, result[idx * column + jdx]); //-20是占了多少字节
        }
        printf("\n");
    }

    /*关闭数据库*/
    sqlite3_close(myDB);
}

int demoStringToInt(const char * str)
{
    #if 0
    int ret = atoi(str);
    printf("ret: %d\n", ret);
    #else

    char *endPtr = NULL;
    int ret = strtol(str, NULL, 10);
    printf("ret: %d\n", ret);
    printf("endptr: %s\n", endPtr);

    #endif


}

int main()
{
    sqlite3 *myDB = NULL;

    /*关闭野指针*/
    const char *filename = "./test.db";

    int ret = sqlite3_open(filename, &myDB);
    if (ret != SQLITE_OK)
    {
        printf("sqlite3_open error:%s\n", sqlite3_errmsg(myDB));
        exit(-1);
    }

    // char name[BUFFER_SIZE] = {0};
    // printf("name:");
    // scanf("%s", name);
    // printf("passwd:");
    // char passwd[BUFFER_SIZE] = {0};
    // scanf("%s", passwd);

    #if 0
    /*注册需求：看用户是否存在*/
    // const char * sql = "select passwd from userInfo where ussername = 'tao'";
    char sql[BUFFER_SIZE] = {0};
    sprintf(sql, "select count(*) from userInfo where ussername = '%s'", name);

    int row = 0;
    int column = 0;
    char **result = NULL;
    /*char *result[]*/
    ret = sqlite3_get_table(myDB, sql, &result, &row, &column, NULL);

    if (ret != SQLITE_OK)
    {
        printf("sqlite3_get_table error:%s\n", sqlite3_errmsg(myDB));
        exit(-1);
    }
    printf("row:%d,\t column: %d\n", row, column);
    printf("result[0]:%s\n", result[0]);
    #else
    /* 登陆功能 - 需求: 看密码是否匹配 */
    const char * sql = "select passwd from userInfo where username = 'bao';";
    int row = 0;
    int column = 0;
    char **result = NULL;
    
    int ret1 = sqlite3_get_table(myDB, sql, &result, &row, &column, NULL);
    if (ret1 != SQLITE_OK)
    {
        printf("sqlite3_get_table error:%s\n", sqlite3_errmsg(myDB));
        exit(-1);
    }
    printf("row:%d,\t column:%d\n", row, column);

    printf("result[0]:%s\n", result[0]);
    printf("result[1]:%s\n", result[1]);
    #endif


    return 0;
}